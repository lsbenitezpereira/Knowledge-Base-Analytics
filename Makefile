.ONESHELL: # Source: https://stackoverflow.com/a/30590240

auth:
	# AzureCLI
	# az login --service-principal \
	# 	--username $(ARM_CLIENT_ID) \
	# 	--password $(ARM_CLIENT_SECRET) \
	# 	--tenant $(ARM_TENANT_ID)

	# az account set --subscription $(ARM_SUBSCRIPTION_ID)
	echo "Login not yet configured"

app-run-local: auth
	echo "go to http://localhost:8888/tree?token=c61a728d-f4e6-45f0-9bb6-65646801f994"
	if docker-compose -f docker-compose.yaml ps | grep notebooks-gateway >/dev/null; then \
		echo "Container is already running."; \
	else \
		echo "Container is not running. Starting it now..."; \
		docker-compose -f docker-compose.yaml up -d; \
	fi;